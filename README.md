# DotenvParser

Simple dotenv style file parser that can parse environment data from file and load it.

See documentation: [https://hexdocs.pm/dotenv_parser](https://hexdocs.pm/dotenv_parser)

## Installation

The package can be installed by adding `dotenv_parser` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:dotenv_parser, "~> 2.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/dotenv_parser](https://hexdocs.pm/dotenv_parser).
